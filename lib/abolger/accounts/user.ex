defmodule Abolger.Accounts.User do
  use Ecto.Schema
  import Ecto.Changeset
  alias Abolger.Repo
  alias Abolger.Sessions.Session
  alias Abolger.Todos.Todo

  schema "users" do
    field :name, :string
    field :email, :string
    field :password_hash, :string
    field :confirm_at, :utc_datetime
    field :reset_sent_at, :utc_datetime
    has_many :sessions, Session, on_delete: :delete_all
    has_many :todos, Todo, on_delete: :delete_all


    timestamps()
  end

    @doc false
    def changeset(user, attrs) do
      user
      |> cast(attrs, [
        :name,
        :email,
        :password_hash
      ])
      |> validate_required([:name, :email, :password_hash])
      |> unique_constraint(:email)
      |> validate_format(:email, ~r/@/)
      |> password_hash()
    end

    defp password_hash(
           %Ecto.Changeset{valid?: true, changes: %{password_hash: password}} = changeset
         ) do
      put_change(changeset, :password_hash, Bcrypt.hash_pwd_salt(password))
    end

    defp password_hash(changeset) do
      changeset
    end



    def get_user!(id), do: Repo.get!(__MODULE__, id)

    def set_require_password_change(user = %{require_password_change: true}, new_password) do
      user =
        user
        |> change(%{require_password_change: true, password_hash: new_password})
        |> password_hash()
        |> Repo.update!()

      {:ok, user}
    end


  end
