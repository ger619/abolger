# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :abolger,
  ecto_repos: [Abolger.Repo]

# Configures the endpoint
config :abolger, AbolgerWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "quN225zBfZQNnmJqnh0n80p4+GRB+8y2QxhlsmavQPT9USUc/lwgyRnmRcPj+ZVT",
  render_errors: [view: AbolgerWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Abolger.PubSub, adapter: Phoenix.PubSub.PG2]

  # Configures Phauxth authentication
  config :phauxth,
    user_context: Abolger.Accounts,
    crypto_module: Bcrypt,
    token_module: AbolgerWeb.Auth.Token


    # Configures mailer
    config :Abolger, AbolgerWeb.Mailer, adapter: Bamboo.LocalAdapter


# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
